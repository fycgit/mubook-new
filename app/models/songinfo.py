from .model import db


class Songinfo(db.Model):
    __tablename__ = 'songinfo'
    Id = db.Column(db.Integer(), nullable=False, primary_key=True)
    Name = db.Column(db.String(128), nullable=True)
    Artist = db.Column(db.String(128), nullable=True)
    Album = db.Column(db.String(128), nullable=True)
    Style = db.Column(db.String(128), nullable=True)
    Style_id = db.Column(db.String(128), nullable=True)
    Url = db.Column(db.String(128), nullable=True)
    Pic_url = db.Column(db.String(128), nullable=True)

    def __init__(self, id, name, artist, album, style, style_id, url, pic_url):
        self.Id = id
        self.Name = name
        self.Artist = artist
        self.Album = album
        self.Style = style
        self.Style_id = style_id
        self.Url = url
        self.Pic_url = pic_url

    def __repr__(self):
        return '<Song %s %s>' % (self.Name, self.Artist)
