from .model import db


class Mubook_c(db.Model):
    __tablename__ = 'mubook_c'
    Id = db.Column(db.Integer(), nullable=False, primary_key=True)
    Number = db.Column(db.Integer(), nullable=False)
    Song_id = db.Column(db.Integer(), nullable=True)
    Song_name = db.Column(db.String(255), nullable=True)
    Song_artist = db.Column(db.String(255), nullable=True)
    Song_album = db.Column(db.String(255), nullable=True)

    def __init__(self, id, number, song_id, name, artist, album):
        self.Id = id
        self.Number = number
        self.Song_id = song_id
        self.Song_name = name
        self.Song_artist = artist
        self.Song_album = album

    def __repr__(self):
        return '<Book No.%d to Song %s>' % (self.Number, self.Song_name)