from .model import db
from werkzeug.security import generate_password_hash, check_password_hash


class User(db.Model):
    __tablename__ = 'userinfo'
    Number = db.Column(db.Integer, primary_key=True)
    Username = db.Column(db.String(100), nullable=False)
    Password = db.Column(db.String(128), nullable=False)
    Gender = db.Column(db.String(10), nullable=False)
    Have_login = db.Column(db.Integer, nullable=False)
    Books_select = db.Column(db.String(128), nullable=True)
    Styles_select = db.Column(db.String(128), nullable=True)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.Password = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.Password, password)

    def __init__(self, num, username, password, gender, Have_login, books_select=None, styles_select=None):
        self.Number = num
        self.Username = username
        self.Password = generate_password_hash(password)
        self.Gender = gender
        self.Have_login = Have_login
        self.Books_select = books_select
        self.Styles_select = styles_select

    def __repr__(self):
        return '<User %s>' % self.Username
