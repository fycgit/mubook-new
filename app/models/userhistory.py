from .model import db


class Userhistory_song(db.Model):
    __tablename__ = 'userhistory_song'
    Number = db.Column(db.Integer(), nullable=False, primary_key=True)
    Username = db.Column(db.String(100), nullable=False)
    Song_id = db.Column(db.Integer(), nullable=True)
    Song_name = db.Column(db.String(255), nullable=True)
    Style = db.Column(db.String(255), nullable=True)

    def __init__(self, number, username, song_id, song_name, style):
        self.Number = number
        self.Username = username
        self.Song_id = song_id
        self.Song_name = song_name
        self.Style = style

    def __str__(self):
        return 'User %s use Song %s'%(self.Username, self.Song_name)


class Userhistory_book(db.Model):
    __tablename__ = 'userhistory_book'
    Number = db.Column(db.Integer(), nullable=False, primary_key=True)
    Username = db.Column(db.String(100), nullable=False)
    Book_number = db.Column(db.Integer(), nullable=True)
    Book_name = db.Column(db.String(255), nullable=True)

    def __init__(self, number, username, book_number, book_name):
        self.Number = number
        self.Username = username
        self.Book_number = book_number
        self.Book_name = book_name

    def __str__(self):
        return 'User %s read Book %s'%(self.Username, self.Book_name)