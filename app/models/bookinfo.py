from .model import db


class Bookinfo(db.Model):
    __tablename__ = "bookinfo"
    Number = db.Column(db.Integer(), nullable=False, primary_key=True)
    Id = db.Column(db.Integer(), nullable=False)
    Name = db.Column(db.String(128), nullable=False)
    Author = db.Column(db.String(128), nullable=False)
    Pic_link = db.Column(db.String(128), nullable=True)
    Press = db.Column(db.String(128), nullable=True)
    Score = db.Column(db.String(128), nullable=True)
    Quote = db.Column(db.Text(), nullable=True)
    Review = db.Column(db.Text(), nullable=True)
    Intro = db.Column(db.Text(), nullable=True)

    def __init__(self, number, id, name, author, piclink, press, score, quote, review, intro):
        self.Number = number
        self.Id = id
        self.Name = name
        self.Author = author
        self.Piclink = piclink
        self.Press = press
        self.Score = score
        self.Quote = quote
        self.Review = review
        self.Intro = intro

    def __str__(self):
        return "<Book %s %s>" % (self.Name, self.Author)
