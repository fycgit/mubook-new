from .model import db


class Bookstyle(db.Model):
    __tablename__ = 'bookstyle'
    Number = db.Column(db.Integer(), nullable=False, primary_key=True)
    Id = db.Column(db.Integer(), nullable=False)
    Name = db.Column(db.String(255), nullable=False)
    Style1 = db.Column(db.String(255), nullable=False)
    Style2 = db.Column(db.String(255), nullable=False)
    Style3 = db.Column(db.String(255), nullable=False)

    def __init__(self, number, id, name, style1, style2, style3):
        self.Number = number
        self.Id = id
        self.Name = name
        self.Style1 = style1
        self.Style2 = style2
        self.Style3 = style3

    def __repr__(self):
        return '<Book %s Style: %s %s %S>' % (self.Name, self.Style1, self.Style2, self.Style3)