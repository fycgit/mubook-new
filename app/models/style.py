from .model import db


class Style(db.Model):
    __table__name = 'Style'
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(10), nullable=False)
    Song_1 = db.Column(db.Integer(), nullable=True)
    Song_2 = db.Column(db.Integer(), nullable=True)
    Song_3 = db.Column(db.Integer(), nullable=True)
    Songname_1 = db.Column(db.String(255), nullable=True)
    Songname_2 = db.Column(db.String(255), nullable=True)
    Songname_3 = db.Column(db.String(255), nullable=True)


    def __init__(self, id, name):
        self.Id = id
        self.Name = name

    def __repr__(self):
        return '<Style %s>' % self.Name
