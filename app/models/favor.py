from .model import db


class Favor(db.Model):
    __tablename__ = 'favor'
    Number = db.Column(db.Integer(), nullable=False, primary_key=True)
    Username = db.Column(db.String(100), nullable=False)
    Book_number = db.Column(db.Integer(), nullable=False)

    def __init__(self, number, username, book_number):
        self.Number = number
        self.Username = username
        self.Book_number = book_number

    def __str__(self):
        return 'User %s favor Book NO.%d'%(self.Username, self.Book_number)