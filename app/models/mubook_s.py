from .model import db


class Mubook_s(db.Model):
    __tablename__ = 'mubook_s'
    Id = db.Column(db.Integer(), nullable=False, primary_key=True)
    Number = db.Column(db.Integer(), nullable=False)
    Song_id = db.Column(db.Integer(), nullable=False)

    def __init__(self, id, number, song_id):
        self.Id = id
        self.Number = number
        self.Song_id = song_id

    def __repr__(self):
        return '<Book No.%d to Song %s>' % (self.Number, self.Song_id)