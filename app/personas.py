import wordcloud
from PIL import Image
import numpy as np
from base64 import urlsafe_b64decode, urlsafe_b64encode
import os


def generate_wordcloud(strings, name, mask):
    basedir = os.path.dirname(__file__)
    with open(os.path.join(basedir, 'static', 'texts', 'stopwords.txt'), 'r', encoding='utf-8') as f:
        stopwords = [x.strip() for x in f.readlines()] + list(wordcloud.STOPWORDS)
    img = Image.open(os.path.join(basedir, 'static', 'img', mask))
    wc = wordcloud.WordCloud(mask=np.array(img), stopwords=stopwords, max_font_size=50,
                             font_path=os.path.join(basedir, 'static', 'texts', 'STXIHEI.TTF'), background_color='white', margin=0)
    wc.generate(' '.join(strings))
    # 由用户名base64加密
    pic_name = '{}.jpg'.format(urlsafe_b64encode(name.encode()).decode())
    wc.to_file(os.path.join(basedir, 'static', 'img', 'personas', pic_name))
    return '/static/img/personas/' + pic_name


if __name__ == '__main__':
    print(os.path.dirname(__file__))