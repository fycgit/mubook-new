import os
import random
import datetime
import os
from flask import Flask, redirect, render_template, request, flash, session, abort
from app.personas import generate_wordcloud

app = Flask(__name__, static_folder='static')

app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://root:123456@localhost:3306/mubook"
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

from app.models.model import db
from app.models.user import User
from app.models.bookinfo import Bookinfo
from app.models.songinfo import Songinfo
from app.models.mubook_c import Mubook_c
from app.models.mubook_s import Mubook_s
from app.models.style import Style
from app.models.bookstyle import Bookstyle
from app.models.userhistory import Userhistory_song, Userhistory_book
from app.models.favor import Favor

app.secret_key = os.urandom(24)

db.create_all()


# todo 复制个图片小点的流畅版
@app.route('/')
def main():
    # 未进行登录则跳转至介绍页面
    if 'login' not in session:
        return redirect('/index')
    userinfo = User.query.filter_by(Username=session['login']).first_or_404()
    if not userinfo.Books_select:
        return redirect('/select/book?username=' + session['login'])
    if not userinfo.Styles_select:
        return redirect('/select/style?username=' + session['login'])

    # 此时用户已经满足了注册、登录、选择爱好的要求，进入主页面
    # todo 搜索
    # todo 风格详情

    # 由当前时间生成导读图片
    now = datetime.datetime.now()
    hour = now.hour
    if hour < 6 or hour > 19:
        pic = '../static/img/time/night.jpg'
    elif hour < 10:  # 6~9
        pic = '../static/img/time/morning.jpg'
    elif hour < 14:  # 10~13
        pic = '../static/img/time/sunny.jpg'
    elif hour < 17:  # 14~16
        pic = '../static/img/time/afternoon.jpg'
    else:  # 17~19
        pic = '../static/img/time/evening.jpg'
    # 随机选择一本书作为导读
    books = Bookinfo.query.all()
    random.Random(123).shuffle(books)  # 以固定随机函数打乱
    n = (now - datetime.datetime(2020, 12, 25)).days % len(books)  # 由当前日期生成一个索引值
    random_book = books[n]
    styles_of_book = Bookstyle.query.filter_by(Number=random_book.Number).first()
    book_styles = [styles_of_book.Style1, styles_of_book.Style2, styles_of_book.Style3]
    recommend_song_id = [mubook_s.Song_id for mubook_s in Mubook_s.query.filter_by(Number=random_book.Number)][:2]
    recommend_song_info = [[song.Id, song.Name] for song in
                           Songinfo.query.filter(Songinfo.Id.in_(recommend_song_id)).all()]
    if list(Favor.query.filter_by(Username=session['login'], Book_number=random_book.Number)):
        favor = 1
    else:
        favor = 0

    # 数据库中用户选择过的书
    book_numbers = User.query.filter_by(Username=session['login']).first().Books_select.split(' ')
    book_numbers = [int(x) for x in book_numbers]
    books = Bookinfo.query.filter(Bookinfo.Number.in_(book_numbers)).all()
    intro = lambda x: x[:100] if len(x) > 100 else x

    bookinfos = [
        [book.Name, intro(book.Intro), '../static/img/bookcovers/%d.jpg' % book.Number, book.Number,
         list(Favor.query.filter_by(Username=session['login'], Book_number=book.Number))]
        for book in books]

    # 数据库中用户选择过的风格
    style_numbers = User.query.filter_by(Username=session['login']).first().Styles_select.split(' ')
    style_numbers = [int(x) for x in style_numbers]
    styles = Style.query.filter(Style.Id.in_(style_numbers)).all()
    # 考虑到Songinfo表的查询效率，将不在主页面展示style所对应的歌曲信息
    styleinfos = [[style.Id, style.Name, [(style.Song_1, style.Songname_1), (style.Song_2, style.Songname_2),
                                          (style.Song_3, style.Songname_3)]] for style in
                  styles]
    return render_template('homepage.html', pic=pic, username=session['login'], guidebook=random_book,
                           book_styles=book_styles,
                           songinfos=recommend_song_info, favor=favor, bookinfos=bookinfos,
                           styleinfos=styleinfos)


# 网站介绍主页，一切开始的地方
@app.route('/index')
def index():
    return render_template('index.html')


# 对于网站特色功能的介绍，在介绍页面和爱好选择页面都会有按钮跳转至此
@app.route('/intro')
def intro():
    return render_template('intro.html')


# 注册
@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        password_r = request.form.get('password_r')
        gender = request.values.get('gender')
        Users = User.query.all()
        if not Users:
            last_user_num = 0
        else:
            last_user_num = Users[-1].Number
        if all([username, password_r, password_r]):
            if username in [user.Username for user in Users]:
                flash('用户名已存在!')
            elif password != password_r:
                flash('两个密码不同')
            else:
                session['register'] = username
                # 存入数据库的密码在User()内由werkzeug加密，细节见user.py
                new_user = User(last_user_num + 1, username, password, gender, 0)
                db.session.add(new_user)
                db.session.commit()
                return redirect('/login')
        else:
            flash('请输入完整信息！')
    return render_template('register.html')


# 登录
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        userinfo = User.query.filter_by(Username=username)
        if userinfo.first() is not None:
            if userinfo.first().verify_password(password):
                # Have_login值为0表示用户是第一次登录
                if userinfo.first().Have_login == 0:
                    # 修改Have_login的值为1
                    user = userinfo.first()
                    user.Have_login = 1
                    db.session.add(user)
                    db.session.commit()
                    session['login'] = username
                    # 第一次登录的用户要跳转至选择兴趣页面
                    return redirect('/select/book?username=%s' % username)
                else:
                    session['login'] = username
                    return redirect('/')
            else:
                flash('密码错误')
        else:
            flash('用户名不存在')
    now = datetime.datetime.now()
    hour = now.hour
    if hour < 6 or hour > 19:
        pic = '../static/img/time/night.jpg'
    elif hour < 10:  # 6~9
        pic = '../static/img/time/morning.jpg'
    elif hour < 14:  # 10~13
        pic = '../static/img/time/sunny.jpg'
    elif hour < 17:  # 14~16
        pic = '../static/img/time/afternoon.jpg'
    else:  # 17~19
        pic = '../static/img/time/evening.jpg'
    return render_template('login.html', pic=pic)


# 选择书籍爱好页面
@app.route('/select/book', methods=['GET', 'POST'])
def book_select():
    # 未登录的用户先登录
    if 'login' not in session:
        return redirect('/login')
    if request.method == 'GET':
        username = request.args.get('username')
        if session['login'] != username:
            abort(404)
        # 随机选12本书
        book_numbers = random.sample(range(1, 251), 12)
        books = Bookinfo.query.filter(Bookinfo.Number.in_(book_numbers)).all()

        bookinfos = [(book.Number, book.Name, '../static/img/bookcovers/%d.jpg' % book.Number, book.Author) for book in
                     books]
        return render_template('select_book.html', bookinfos=bookinfos)

    if request.method == 'POST':
        values_dict = request.values.dicts[1]
        chosed_books = dict(values_dict).keys()
        if len(chosed_books) != 4:
            flash('请选择4本书籍')
            return redirect('/select/book?username=' + session['login'])
        userinfo = User.query.filter_by(Username=session['login']).first_or_404()
        userinfo.Books_select = ' '.join(chosed_books)
        db.session.add(userinfo)
        db.session.commit()
        # 未进行过风格选择则跳转至风格选择页面
        if not userinfo.Styles_select:
            return redirect('/select/style?username=' + session['login'])
        return redirect('/')


# 选择音乐风格爱好页面
@app.route('/select/style', methods=['GET', 'POST'])
def style_select():
    # 未登录的用户先登录
    if 'login' not in session:
        return redirect('/login')

    if request.method == 'GET':

        if 'login' not in session:
            return redirect('/login')
        username = request.args.get('username')
        if session['login'] != username:
            abort(404)

        styles = Style.query.all()
        styles = random.sample(styles, 10)
        styleinfos = [(style.Id, style.Name, '../static/img/style_pics/{}.jpg'.format(style.Id))
                      for style in styles]
        return render_template('select_style.html', styleinfos=styleinfos)

    if request.method == 'POST':
        values_dict = request.values.dicts[1]
        chosed_styles = dict(values_dict).keys()
        if len(chosed_styles) != 4:
            flash('请选择4种音乐分类')
            return redirect('/select/style?username=' + session['login'])
        userinfo = User.query.filter_by(Username=session['login']).first_or_404()
        userinfo.Styles_select = ' '.join(chosed_styles)
        db.session.add(userinfo)
        db.session.commit()
        return redirect('/')


@app.route('/personas', methods=['GET'])
def personas():
    # 没登录直接404
    if 'login' not in session:
        abort(404)
    song_histories = list(Userhistory_song.query.filter_by(Username=session['login']))
    book_histories = list(Userhistory_book.query.filter_by(Username=session['login']))
    gender = User.query.filter_by(Username=session['login']).first().Gender
    # 词云形状由性别决定
    if gender == 'Man':
        mask = 'man.png'
    else:
        mask = 'woman.png'
    if not song_histories:
        return "0"
    songnames = [history.Song_name for history in song_histories]
    # todo styles的展示
    # todo 书的词云
    styles = [history.Style for history in song_histories]
    return generate_wordcloud(songnames, session['login'], mask)


@app.route('/book/<int:number>')
def book(number):
    book = Bookinfo.query.filter_by(Number=number)
    # 如果Number为number的书不存在，404
    if not book:
        abort(404)
    book = book.first()
    bookinfo = [book.Number, book.Name, book.Author, book.Press, book.Intro.split('\n'), book.Quote.split('\n'),
                book.Review.split('\n'), '../static/img/bookcovers/{}.jpg'.format(book.Number)]

    songs_c = Mubook_c.query.filter_by(Number=number)[:8]
    songinfos = [['https://music.163.com/#/song?id=%d' % song.Song_id, song.Song_name,
                  song.Song_artist, song.Song_album] for song in songs_c]
    is_logged = int('login' in session)
    if is_logged:
        # 是否被收藏
        is_favored = 1 if list(Favor.query.filter_by(Username=session['login'], Book_number=bookinfo[0])) else 0
        bookinfo.append(is_favored)
    return render_template('bookinfo.html', bookinfo=bookinfo, songinfos=songinfos, is_logged=is_logged)


@app.route('/record/song', methods=['GET'])
def record_song():
    song_id = request.args.get('id')
    try:
        song = Songinfo.query.filter_by(Id=song_id).first()
        name = song.Name
        style = song.Style
        try:
            last_history = Userhistory_song.query.all()[-1]
            number = last_history.Number + 1
        except:
            number = 1
        new_history = Userhistory_song(number, session['login'], song_id, name, style)
        db.session.add(new_history)
        db.session.commit()
        return 'OK'
    except:
        return 'Song not found'


@app.route('/record/book', methods=['GET'])
def record_book():
    book_number = request.args.get('number')
    try:
        name = Bookinfo.query.filter_by(Number=book_number).first().Name
        try:
            last_history = Userhistory_book.query.all()[-1]
            number = last_history.Number + 1
        except:
            number = 1
        new_history = Userhistory_book(number, session['login'], book_number, name)
        db.session.add(new_history)
        db.session.commit()
        return 'OK'
    except:
        return 'Book not found'


@app.route('/favorites/change', methods=['GET'])
def favorites_change():
    # 没登录就进入此网址跳转至登录页面
    if 'login' not in session:
        return redirect('/login')
    book_number = request.args.get('book_number')
    method = request.args.get('method')
    try:
        number = Favor.query.all()[-1].Number + 1  # 最后一条记录序号加一
    except:
        number = 1  # 没有记录则序号为1
    if method == 'add':  # 加
        if list(Favor.query.filter_by(Username=session['login'], Book_number=book_number)):
            return 'Already favored'
        favor = Favor(number, session['login'], book_number)
        db.session.add(favor)
        db.session.commit()
    elif method == 'remove':  # 去掉
        favor = Favor.query.filter_by(Username=session['login'], Book_number=book_number).first()
        db.session.delete(favor)
        db.session.commit()
    else:
        abort(404)
    return 'OK'


@app.route('/favorites', methods=['GET'])
def favorites():
    item_per_page = 4  # 每页多少item
    if 'login' not in session:
        return redirect('/login')
    page_arg = request.args.get('page')
    if not page_arg:  # page为None或为0时404
        abort(404)
    page = int(page_arg)
    favors = Favor.query.filter_by(Username=session['login'])
    amount = favors.count()
    last_page = amount // item_per_page + 1 if amount % item_per_page != 0 else amount // item_per_page
    is_last_page = last_page == page
    if 1 <= page <= last_page:
        if page == last_page:
            book_numbers = [favor.Book_number for favor in favors[page * item_per_page - item_per_page:]]
        else:
            book_numbers = [favor.Book_number for favor in
                            favors[page * item_per_page - item_per_page:page * item_per_page]]
        books = Bookinfo.query.filter(Bookinfo.Number.in_(book_numbers)).all()
        intro = lambda x: x[:100] if len(x) > 100 else x
        bookinfos = [
            [book.Name, intro(book.Intro), '../static/img/bookcovers/%d.jpg' % book.Number, book.Number,
             list(Favor.query.filter_by(Username=session['login'], Book_number=book.Number))]
            for book in books]
        links = [(n, '/favorites?page=%d#book' % n) for n in range(1, last_page + 1)]
        return render_template('favorites.html', page=page, bookinfos=bookinfos, links=links, is_last_page=is_last_page,
                               next_page=page + 1, prev_page=page - 1)
    else:
        abort(404)


@app.route('/booksong', methods=['GET'])
def booksong():
    book_number = request.args.get('number')
    book = list(Bookinfo.query.filter_by(Number=book_number))
    if not book:
        abort(404)
    bookinfo = (book_number, book[0].Name)
    ids = [x.Song_id for x in Mubook_s.query.filter_by(Number=book_number)[:8]]
    songs = Songinfo.query.filter(Songinfo.Id.in_(ids)).all()
    songinfos = [[song.Id, song.Name, song.Artist, song.Album, song.Style, song.Pic_url] for song in songs]
    return render_template('booksong.html', bookinfo=bookinfo, songinfos=songinfos)


@app.route('/style', methods=['GET'])
def style():
    if 'login' not in session:
        return redirect('/login')
    song_num = 8  # 一次展示歌的个数
    style_name = request.args.get('name')
    try:
        style_select = Style.query.filter_by(Name=style_name).first()
        styles = Style.query.all()
        other_styles = [style.Name for style in styles if style.Name != style_name]
    except:
        abort(404)
    songs = list(Songinfo.query.filter_by(Style=style_name).all())
    total_num = len(songs)
    random_songs = random.sample(songs, song_num)
    songinfos = [[song.Id, song.Name, song.Artist, song.Album] for song in random_songs]
    return render_template('style.html', songinfos=songinfos, total_num=total_num, style_name=style_name,
                           other_styles=other_styles)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html')
